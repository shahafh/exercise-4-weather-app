import React from 'react';
import { useSelector } from 'react-redux';
import { selectors } from './store';
import City from './City';

const Home = () => {
  const cities = useSelector(selectors.getCities);

  return (
    <main>
      <ul>
        {cities.map((city) => (
          <City key={city.name} name={city.name} />
        ))}
      </ul>
    </main>
  );
};
Home.propTypes = {};
export default Home;
