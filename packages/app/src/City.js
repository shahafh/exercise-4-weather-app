import React from 'react';
import { useSelector } from 'react-redux';
import { selectors } from './store';

const City = ({ name }) => {
  // select itself from the state
  // if it doesn't have a temperature go fetch it
  // if you are pending, do nothing!
  const city = useSelector(selectors.getCity(name));
  console.log({ city });
  return <li>{name}</li>;
};

City.propTypes = {};

export default City;
