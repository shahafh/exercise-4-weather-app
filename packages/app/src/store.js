import { configureStore, combineReducers } from '@reduxjs/toolkit';
import cities from './cities.reducer';

const reducer = combineReducers({
  cities: cities.reducer,
});

/**
 * @typedef {import('./cities.reducer').CitiesState} CitiesState
 * @typedef {import('./cities.reducer').City} City
 * @typedef {{
 *  cities: CitiesState
 * }} State
 * */

const store = configureStore({
  reducer,
});

export const actions = cities.actions;
export const selectors = {
  /** @param {State} state @returns {City[]} */
  getCities: (state) => cities.selectors.selectAll(state.cities.collection),
  /** @param {string} id  @returns {(state: State) => City} */
  getCity: (id) => (state) =>
    cities.selectors.selectById(state.cities.collection, id),
  /** @param {State} state */
  getIsPending: (state) => state.cities.isPending,
};

export default store;
