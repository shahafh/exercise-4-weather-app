import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
  combineReducers,
} from '@reduxjs/toolkit';
import axios from 'axios';

/** @typedef {{
 *  name: string,
 *  main?: {
 *    temp: 21.19
 *  }
 * }} City
 *
 * @typedef {{ ids: string[], entities: Object.<string,City> }} CollectionState
 *
 * @typedef {{
 *  collection: CollectionState,
 *  isPending: boolean
 * }} CitiesState
 * */
const { REACT_APP_WEATHER_API_KEY: API_KEY } = process.env;

const citiesEntityAdapter = createEntityAdapter({
  selectId: (item) => item.name,
});

const fetchCity = createAsyncThunk(
  'cities/fetchCity',
  /** @param {{ name: string }} payload @returns {Promise<City | null>}*/
  async (payload, thunkApi) => {
    const url = `api.openweathermap.org/data/2.5/weather?q=${payload.name}&appid=${API_KEY}&units=metric`;
    let response;
    try {
      response = await axios.get(url);
    } catch (error) {
      thunkApi.rejectWithValue(error);
      return null;
    }
    /** @type {{ data: City }} */
    const { data } = response;
    return data;
  }
);

const collection = createSlice({
  name: 'cities',
  /** @type {CollectionState} */
  initialState: { ids: ['London'], entities: { London: { name: 'London' } } },
  reducers: {
    /** @param {{ payload: {name: string} }} action */
    addCity: (state, action) =>
      citiesEntityAdapter.addOne(state, action.payload),
    /** @param {{ payload: {name: string} }} action */
    dropCity: (state, action) =>
      citiesEntityAdapter.removeOne(state, action.payload.name),
  },
  extraReducers: {
    /** @param {{payload: City}} action */
    [fetchCity.fulfilled.toString()]: (state, action) =>
      citiesEntityAdapter.updateOne(state, {
        id: action.payload.name,
        changes: action.payload,
      }),
  },
});

const isPending = createSlice({
  name: 'cities/pendning',
  initialState: false,
  reducers: {},
  extraReducers: {
    [fetchCity.pending.toString()]: () => true,
    [fetchCity.fulfilled.toString()]: () => false,
    [fetchCity.rejected.toString()]: () => false,
  },
});

export default {
  reducer: combineReducers({
    collection: collection.reducer,
    isPending: isPending.reducer,
  }),
  selectors: citiesEntityAdapter.getSelectors(),
  actions: { ...collection.actions, fetchCity },
};
