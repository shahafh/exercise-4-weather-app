import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Home from './Home';
import Form from './Form';

console.log(process.env.REACT_APP_WEATHER_API_KEY);

const App = () => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="add" component={Form} />
      </Switch>
    </Router>
  </Provider>
);

export default App;
